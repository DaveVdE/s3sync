﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Gemini.CommandLine;

namespace s3sync
{
    class Program
    {
        static void Main(string[] args)
        {
            Command.FromArguments(args).Run(typeof (Sync));
        }
    }
}
