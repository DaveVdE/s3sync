﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Amazon;
using Amazon.S3;
using Amazon.S3.Model;
using Gemini.CommandLine;

namespace s3sync
{
    class BucketInfo
    {
        public string Name { get; set; }
        public string Location { get; set; }
    }

    public class Sync
    {
        private const long PartSize = 5 * 1024 * 1024;
        private string Key { get; set; }
        private string Secret { get; set; }
        private AmazonS3 Client { get; set; }

        private static readonly Dictionary<string, string> LocationToEndPoint = new Dictionary<string, string>
        {
            {"EU", "s3-eu-west-1.amazonaws.com"},
            {"eu-west-1", "s3-eu-west-1.amazonaws.com"},
            {"US", "s3.amazonaws.com"}
        };

        public Sync(string key, string secret)
        {
            Key = key;
            Secret = secret;

            var config = new AmazonS3Config();

#if DEBUG
            config.WithCommunicationProtocol(Protocol.HTTP);
#endif

            Client = AWSClientFactory.CreateAmazonS3Client(key, secret, config);
        }

        private void CreateRegionClient(string location)
        {
            var config = new AmazonS3Config()
                .WithServiceURL(LocationToEndPoint[location])
                .WithCommunicationProtocol(Protocol.HTTP);

#if DEBUG
            config.WithCommunicationProtocol(Protocol.HTTP);
#endif

            Client = AWSClientFactory.CreateAmazonS3Client(Key, Secret, config);
        }

        public void ListBuckets()
        {
            Console.WriteLine("Fetching buckets...");

            var buckets = ListBucketsAsync().ContinueWith(_ => Task.WhenAll(_.Result.Select(DescribeBucketAsync)).Result);

            foreach (var info in buckets.Result)
            {
                Console.WriteLine("Bucket: {0}, location: {1}.", info.Name, info.Location);
            }
        }

        private async Task<IEnumerable<S3Object>> ListObjectsAsync(string bucketName, string prefix)
        {
            ListObjectsResponse response = null;
            var lists = new List<List<S3Object>>();

            while (response == null || response.IsTruncated)
            {
                var request = new ListObjectsRequest().WithBucketName(bucketName).WithPrefix(prefix);

                if (response != null)
                {
                    request.WithMarker(response.NextMarker);
                }

                response = await Task<ListObjectsResponse>.Factory.FromAsync(
                    Client.BeginListObjects, Client.EndListObjects, request, null);

                lists.Add(response.S3Objects);
            }

            return lists.SelectMany(l => l);
        }

        public void ListObjects(string bucket, string prefix)
        {
            var info = DescribeBucketAsync(bucket).Result;
            CreateRegionClient(info.Location);

            foreach (var s3object in ListObjectsAsync(bucket, prefix).Result)
            {
                Console.WriteLine("Key: {0}, Size: {1}", s3object.Key, s3object.Size);
            }
        }

        private static string PathToKeyName(string path, string basePath)
        {
            return path.Substring(basePath.Length).Replace('\\', '/');
        }

        private static IEnumerable<FileInfo> ListDirectory(string directory)
        {
            return Directory.EnumerateFiles(directory, "*.*", SearchOption.AllDirectories).Select(fileName => new FileInfo(fileName));
        }

        private async Task<Dictionary<string, S3Object>> CreateBucketDictionary(string bucket, string prefix)
        {
            var list = await ListObjectsAsync(bucket, prefix);

            return list.ToDictionary(item => item.Key);
        }

        private class SyncAction
        {
            public FileInfo File { get; set; }
            public string Key { get; set; }
        }

        private async Task<List<SyncAction>> AnalyzeAsync(string bucket, string local, string prefix)
        {
            var dictionaryTask = CreateBucketDictionary(bucket, prefix);
            var files = ListDirectory(local).ToArray();

            var objects = await dictionaryTask;
            var actions = new List<SyncAction>(files.Length);

            foreach (var file in files)
            {
                S3Object match;
                var key = prefix + PathToKeyName(file.FullName, local);
                if (!objects.TryGetValue(key, out match))
                {
                    actions.Add(new SyncAction
                    {
                        File = file,
                        Key = key
                    });
                }
            }

            return actions;
        }

        public void Analyze(string bucket, string local, string prefix)
        {
            Console.WriteLine("Analyzing...");

            var actions = AnalyzeAsync(bucket, local, prefix).Result;

            Console.WriteLine("Missing files: {0}, total: {1}", actions.Count, BytesToString(actions.Sum(action => action.File.Length)));
        }

        private static long FromMax(string max)
        {
            long factor = 1;

            if (max.EndsWith("g", StringComparison.InvariantCultureIgnoreCase))
            {
                factor = 1000000000;
                max = max.Substring(0, max.Length - 1);
            }
            else if (max.EndsWith("m", StringComparison.InvariantCultureIgnoreCase))
            {
                factor = 1000000;
                max = max.Substring(0, max.Length - 1);
            }
            else if (max.EndsWith("k", StringComparison.InvariantCultureIgnoreCase))
            {
                factor = 1000;
                max = max.Substring(0, max.Length - 1);
            }

            return long.Parse(max)*factor;
        }

        private void CopyInParts(FileInfo file, string bucket, string key)
        {
            var initiateRequest = new InitiateMultipartUploadRequest()
                .WithBucketName(bucket)
                .WithKey(key);

            var initiateResponse = Client.InitiateMultipartUpload(initiateRequest);
            var stopwatch = new Stopwatch();
            stopwatch.Start();

            var partNumber = 1;
            var uploadResponses = new List<UploadPartResponse>();
            for (long position = 0; position < file.Length; position += PartSize, ++partNumber)
            {
                var uploadRequest = new UploadPartRequest()
                    .WithBucketName(bucket)
                    .WithKey(key)
                    .WithFilePath(file.FullName)
                    .WithFilePosition(position)
                    .WithPartNumber(partNumber)
                    .WithPartSize(PartSize)
                    .WithUploadId(initiateResponse.UploadId);

                uploadRequest.UploadPartProgressEvent += (s, e) =>
                {
                    var elapsed = stopwatch.Elapsed;

                    if (!(elapsed.TotalSeconds > 0)) return;

                    var transferred = e.TransferredBytes + position;
                    var speed = transferred / elapsed.TotalSeconds;
                    var eta = TimeSpan.FromSeconds((file.Length - transferred) / speed);

                    Console.Write("Part complete: {0}%/{1}, Elapsed: {2:d\\.hh\\:mm\\:ss}, Speed: {3}/s, ETA: {4:d\\.hh\\:mm\\:ss}                         \r", e.PercentDone, BytesToString(transferred), elapsed, BytesToString(speed), eta);
                };

                var response = Client.UploadPart(uploadRequest);
                uploadResponses.Add(response);
            }

            var completeRequest = new CompleteMultipartUploadRequest()
                .WithBucketName(bucket)
                .WithKey(key)
                .WithPartETags(uploadResponses)
                .WithUploadId(initiateResponse.UploadId);

            Client.CompleteMultipartUpload(completeRequest);
        }

        private string BytesToString(double bytes)
        {
            if (bytes > 1024*1024)
            {
                return string.Format("{0:F2}MB", bytes/(1024*1024));
            }
            if (bytes > 1024)
            {
                return string.Format("{0:F2}KB", bytes/1024);
            }
            return string.Format("{0:F2}B", bytes);
        }        

        private void Copy(FileInfo file, string bucket, string key)
        {
            var request = new PutObjectRequest()
                .WithBucketName(bucket)
                .WithKey(key)
                .WithFilePath(file.FullName);

            var stopwatch = new Stopwatch();
            stopwatch.Start();

            request.PutObjectProgressEvent += (s, e) =>
            {
                TimeSpan elapsed = stopwatch.Elapsed;

                if (!(elapsed.TotalSeconds > 0)) return;

                var transferred = e.TransferredBytes;
                var speed = transferred/elapsed.TotalSeconds;
                var eta = TimeSpan.FromSeconds((e.TotalBytes - transferred)/speed);

                Console.Write("Complete: {0}%/{1}, Elapsed: {2:d\\.hh\\:mm\\:ss}, Speed: {3}/s, ETA: {4:d\\.hh\\:mm\\:ss}                       \r", e.PercentDone, BytesToString(transferred), elapsed, BytesToString(speed), eta);
            };

            Client.PutObject(request);
        }

        public void Synchronize(string bucket, string local, string prefix, string max, [Argument("at", true)] string at)
        {
            if (!string.IsNullOrEmpty(at))
            {
                var future = DateTime.Parse(at);

                if (future.Date == DateTime.MinValue)
                {
                    future = DateTime.Today + TimeSpan.Parse(at);
                }

                if (future < DateTime.Now)
                {
                    future = future.AddDays(1);
                }

                while (future > DateTime.Now)
                {
                    Console.Write("Starting in {0:d\\.hh\\:mm\\:ss}.                     \r", future - DateTime.Now);
                    Thread.Sleep(1000);
                }
            }

            long maxBytes = FromMax(max);

            var actions = AnalyzeAsync(bucket, local, prefix).Result;

            foreach (var action in actions)
            {
                Console.WriteLine("[{1}] Copying {0}...                                                        ", action.File.FullName, DateTime.Now);

                if (action.File.Length > maxBytes)
                {
                    break;
                }

                if (action.File.Length > PartSize)
                {
                    CopyInParts(action.File, bucket, action.Key);
                }
                else
                {
                    Copy(action.File, bucket, action.Key);    
                }                               

                maxBytes -= action.File.Length;
            }
        }

        private async Task<IEnumerable<string>> ListBucketsAsync()
        {
            var response = await Task<ListBucketsResponse>.Factory.FromAsync(
                Client.BeginListBuckets, Client.EndListBuckets, new ListBucketsRequest(), null);

            return response.Buckets.Select(bucket => bucket.BucketName);
        }

        private async Task<BucketInfo> DescribeBucketAsync(string bucketName)
        {
            var response = await Task<GetBucketLocationResponse>.Factory.FromAsync(
                Client.BeginGetBucketLocation, Client.EndGetBucketLocation, new GetBucketLocationRequest().WithBucketName(bucketName), null);

            return new BucketInfo
            {
                Name = bucketName,
                Location = response.Location
            };
        }
    }
}
